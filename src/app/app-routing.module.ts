import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// App
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'about',
    loadChildren: '../app/about/about.module#AboutModule'
  },
  {
    path: 'about-us',
    loadChildren: '../app/about/about.module#AboutModule'
  },
  {
    path: 'order',
    loadChildren: '../app/order/order.module#OrderModule'
  },
  // fallback to a specific route
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    { useHash: false, initialNavigation: 'enabled' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
