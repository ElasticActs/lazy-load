import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Pipe({
  name: 'trustAsHtml'
})
export class TrustAsHtmlPipe implements PipeTransform {
  constructor(
    private trustAsHtml: DomSanitizer) {
  }

  transform(value: any): SafeHtml {
    const regex = /&#38;/gi;
    if (regex.test(value)) {
      value = value.replace(regex, '&');
    }

    return this.trustAsHtml.bypassSecurityTrustHtml(value);
  }
}
