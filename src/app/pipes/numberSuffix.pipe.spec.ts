import { NumberSuffixPipe } from './numberSuffix.pipe';

describe('NumberSuffixPipe', () => {
  it('change date format', () => {
    const pipe = new NumberSuffixPipe();
    expect(pipe.transform('2017-11-10')).toBe('17/11/10');
  });
});
