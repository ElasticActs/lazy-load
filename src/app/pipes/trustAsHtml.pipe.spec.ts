import { TrustAsHtmlPipe } from './trustAsHtml.pipe';

describe('TrustAsHtmlPipe', () => {
  const value: any = '<a>link</a>';

  xit('change special characters ', () => {
    const pipe = new TrustAsHtmlPipe(value);
    expect(pipe.transform('&lpar;Trust &num; as &#38; Html&rpar;')).toBe('(Trust # as & Html');
  });
});
