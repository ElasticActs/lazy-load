import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { OrderRoutingModule } from './order-routing.module';
import { OrderComponent } from './order.component';

import { NumberSuffixPipe } from '../pipes/numberSuffix.pipe';
@NgModule({
  declarations: [
    OrderComponent,
    NumberSuffixPipe],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    OrderRoutingModule
  ]
})
export class OrderModule { }
