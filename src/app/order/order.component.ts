import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { ConnectionService } from '../services/connection.service';
import { CartService } from '../services/cart.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
declare var window: any;

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
@Injectable()
export class OrderComponent implements OnInit {
  miniBC: any = {};
  errorInfo: any = {
    flag: false,
    message: '1'
  };
  quantityErrorMsg = 'Quantity is required.';
  typeIsbnErrorMsg = 'Please type ISBN to correct.';
  incorrectLengthErrorMsg = 'Incorrect ISBN Length';
  mainErrorStatus = false;
  windowPasteEvent = false;
  availableStatus: any = [false];
  deleteBtnStatus: any = [true];
  submitted = false;
  initialISBN: any = [];
  individualISBN: any = [];
  isbnString: any;
  qtyNumber: number;
  isbnListResult: any = [];
  expandStatus: any = [];
  extraEmptyGroup = ' \t';
  calculatedId: any = [];
  subTotal = 0;
  form: FormGroup;
  restrictedText: string ;
  restrictedCodes = ['AUS', 'CDN', 'WOR', 'USX', 'CSO', 'CTR', 'DND', 'NAM', 'NGR'];
  customerId: number;

  constructor(
    @Inject(DOCUMENT) private document: any,
    private connection: ConnectionService,
    private fb: FormBuilder,
    private cart: CartService
  ) {
    this.miniBC = {
      storeID: environment.storeID,
      token: environment.token,
      url: environment.url
    };
   }

   ngOnInit() {
    this.form = this.fb.group({
      product_id: '',
      isbn: '',
      quantity: '',
      isbnLists: this.fb.array([])
    });

    this.customerId = this.document.getElementsByTagName('head')[0].getAttribute('data-customer-id');
  }

  get isbnListForms() {
    return this.form.get('isbnLists') as FormArray;
  }

  /**
   * Process ISBN Group pasted
   */
  public onMultiISBN(event: ClipboardEvent) {
    const pastedText =  this.getClipboardData(event.clipboardData);

    this.initialISBN = pastedText.split(/\n/g).filter(Boolean);
    if (this.initialISBN[0].replace(/\s/g, '') !== '') {
      this.initialISBN.push(this.extraEmptyGroup);
      this.processIsbn(this.initialISBN, true);
    }
  }

  /**
   * Single ISBN
   */
  public onSingleISBN(id: number) {
    const singleIsbn = this.document.getElementById('initialISBN_' + id).value;
    const singleQty = this.document.getElementById('isbnQty_' + id);

    if (singleIsbn) {
      const singleIsbnArray = [singleIsbn + '\t', this.extraEmptyGroup];
      this.processIsbn(singleIsbnArray, false);
    }

    if (!singleQty && singleIsbn.length > 9) {
      this.moveFocus('isbnQty_' + id);
    }
  }

  /**
   * Process ISBNs
   */
  private processIsbn(isbnWithQty: any, multi: boolean) {
    switch (true) {
      case (isbnWithQty.length >= 1):
        isbnWithQty.forEach((item: any, index: number) => {
          // Initialize expand btn and error info
          this.expandStatus[index] = false;
          this.errorInfo = {
            flag: false,
            message: '3'
          };

          // Check length of individual ISBN
          // if length is bigger than 2, it means it has more than isbn, qty
          this.individualISBN = item.split(/\t/g).filter(Boolean);
          if (this.individualISBN.length > 2 && isbnWithQty.length - 1 !== index) {
            this.mainErrorStatus = true;
            this.errorInfo = {
              flag: true,
              message: 'The data is in the wrong format.'
            };
          }

          // Check length of isbn 10 or 13
          this.isbnString = this.individualISBN[0].replace(/[^xX0-9]/g, '');
          if ((this.isbnString.length !== 10 && this.isbnString.length !== 13) && isbnWithQty.length - 1 !== index) {
            this.mainErrorStatus = true;
            this.errorInfo = {
              flag: true,
              message: this.incorrectLengthErrorMsg
            };
          }

          const qtyAfterTrim = this.individualISBN[this.individualISBN.length - 1].replace(/^\s+|\s+$/gm, '');
          this.qtyNumber = this.individualISBN.length === 1 ? '' : qtyAfterTrim;

          const isbnData: any = {};
          isbnData.product_id = '';
          isbnData.number = this.isbnString;
          isbnData.qty = this.qtyNumber;
          isbnData.error = this.errorInfo;
          isbnData.info = [];

          this.isbnListResult.push(isbnData);
        });
        break;
      default:
        this.isbnListResult = {
          number: '',
          qty: '',
          error: {
            flag: true,
            message: 'Something is wrong with your data'
          }
        };
    }

    this.getMultiProductInfo(0).subscribe(result => {
      console.log(this.isbnListResult);

      // Need to re initialize form
      this.ngOnInit();

      this.addISBN(this.isbnListResult);

       // If no error, move focus to last isbn number input
      if (multi) {
        if (!this.mainErrorStatus) {
          if (this.isbnListResult.length === 2 && this.isbnListResult[0].qty === '') {
            this.moveFocus('isbnQty_0');
          } else {
            const lastId = isbnWithQty.length - 1;
            this.moveFocus('isbnNumber_' + lastId);
          }
        }
      }
    });
  }

  /**
   * Add product information
   */
  private getMultiProductInfo(index: number): Observable<any> {
    if (this.isbnListResult[index].error.flag && this.isbnListResult[index].number === '') {
      return new Observable(observer => {
        if (index < this.isbnListResult.length - 1) {
          this.getMultiProductInfo(index + 1).subscribe(result => {
            observer.next();
            observer.complete();
          });
        } else {
          observer.next();
          observer.complete();
        }
      });
    } else {
      const getValues = '?storeID=' + this.miniBC.storeID + '&token=' + this.miniBC.token + '&isbn=' + this.isbnListResult[index].number;

      return new Observable(observer => {
        if (index < this.isbnListResult.length - 1) {
          this.connection.get(this.miniBC.url + '/apps/nelson/storefront/getProductInfo' + getValues).subscribe(data => {
            if ('error' in data) {
              this.isbnListResult[index].info = [];
              if (!this.isbnListResult[index].error.flag) {
                this.isbnListResult[index].error = {
                  flag: true,
                  message: this.isbnListResult[index].number + ' does not match with any ISBNs.'
                };
              }
            } else {
              if (parseInt(data.available_qty, 10) < parseInt(this.isbnListResult[index].qty, 10)) {
                const backOrderQty = this.isbnListResult[index].qty - data.available_qty;
                data.backOrder = backOrderQty;
              }

              this.isbnListResult[index].product_id = data.product_id;
              this.isbnListResult[index].info = data;
              this.setRestrictedItem(index, data);
            }

            this.getMultiProductInfo(index + 1).subscribe(result => {
              observer.next();
              observer.complete();
            });

          });
        } else {
          observer.next();
          observer.complete();
        }
      });
    }
  }

  /**
   * Add new isbn group
   */
  private addNewIsbnGroup(id: number) {
    const newIndex = id + 1;
    const newGroup = this.document.getElementById('isbnGroup_' + newIndex);
    if (newGroup === null) {
      this.deleteBtnStatus[this.isbnListForms.value.length - 1] = true;
      this.expandStatus[newIndex] = false;

      this.errorInfo = {
        flag: false,
        message: '2'
      };

      const isbnData = {
        product_id: '',
        number: '',
        qty: '',
        error: this.errorInfo,
        info: ''
      };

      this.isbnListResult.push(isbnData);
      const isbn = this.fb.group({
        product_id: [],
        isbn: [],
        quantity: [],
        status: ['false'],
        info: []
      });

      this.isbnListForms.push(isbn);

      this.deleteBtnStatus[this.isbnListForms.value.length - 1] = false;
    }
  }

  /**
   * Add isbn and qty from ISBN List
   */
  private addISBN(isbnList: any) {
    this.subTotal = 0;
    isbnList.forEach((item: any, index: number) => {
      this.deleteBtnStatus[index] = true;

      const isbn = this.fb.group({
        product_id: [item.info.product_id],
        isbn: [item.number],
        quantity: [item.qty],
        status: [item.error],
        info: [item.info]
      });

      this.isbnListForms.push(isbn);

      if (!item.error.flag && item.number !== '') {
        this.updateSubTotal(item.info.net_price, item.qty, index);
      }
    });
    // Set button not to show for last one
    this.deleteBtnStatus[this.isbnListForms.value.length - 1] = false;
  }

  /**
   * Remove isbn and qty
   */
  public deleteIsbn(id: number) {
    // Re calculate subtotal, if it has no error
    if (!this.isbnListResult[id].error.flag && !this.isbnListResult[id].restrictedStatus) {
      const isbnQty = this.getQtyValue(id);
      const itemTotal = this.isbnListResult[id].info.net_price * isbnQty;
      this.subtractTotal(itemTotal);
    }

    this.isbnListForms.removeAt(id);
    this.isbnListResult.splice(id, 1);

    if (this.isbnListResult.length === 1) {
      this.subTotal = 0;
      this.isbnListResult = [];
      this.ngOnInit();
      this.calculatedId = [];
    } else {
      this.calculatedId = this.calculatedId.filter(item => item !== id);
      // Reset for delete Button for last one
      this.deleteBtnStatus[this.isbnListForms.value.length - 1] = false;
    }
  }

  /**
   * Move cursor to focus
   */
  private moveFocus(id: string) {
    setTimeout(() => {
      this.document.getElementById(id).focus();
    }, 200);
  }

  /**
   * Expand Book information panel
   */
  public getExpandInfo(id: number) {
    // This is to hide "Available" text when the screen width is small
    if (this.document.getElementById('isbnAvailable_' + id)) {
      this.expandStatus[id] = !this.expandStatus[id];
      if (this.expandStatus[id]) {
        this.document.getElementById('isbnAvailable_' + id).classList.add('hideText');
      } else {
        this.document.getElementById('isbnAvailable_' + id).classList.remove('hideText');
      }
    }
  }

  /**
   * Get isbn input value by ID
   */
  private getIsbnValue(id: number) {
    const newISBN = this.document.getElementById('isbnNumber_' + id).value;

    return newISBN.replace(/[^0-9]/g, '');
  }

  /**
   * Get qty input value by ID
   */
  private getQtyValue(id: number) {
    const tempQty = parseInt(this.document.getElementById('isbnQty_' + id).value, 10);

    return Number.isNaN(tempQty) ? 0 : tempQty;
  }

  /**
   * Checking the length of ISBN
   */
  public treatIsbnInfo(event: any, id: number) {
    // Detect Browser and window paste event
    const isIEOrEdge = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent);
    if (isIEOrEdge) {
      window.addEventListener('paste', (event) => {
        this.windowPasteEvent = true;
      });
    }

    // Determined type or paste
    if (typeof event.clipboardData === 'undefined' && !this.windowPasteEvent && event.keyCode !== 93) {
      console.log('type');

      if (this.getIsbnValue(id).length > 3 && this.getIsbnValue(id).length < 10 && this.checkKeyCode(event.keyCode)) {
        this.isbnListResult[id].error = {
          flag: true,
          message: this.incorrectLengthErrorMsg
        };
        this.addNewIsbnGroup(id);
      }

      // Check input length to Process, it has to be bigger than 9
      if (this.getIsbnValue(id) !== '' && this.getIsbnValue(id).length > 9 && this.checkKeyCode(event.keyCode)) {
        this.updateSubTotalWithIsbnUpdate(id);

        this.addNewIsbnGroup(id);
        this.processSingle(id);
      }
    } else if (event.clipboardData || this.windowPasteEvent) {
      console.log('copy');

      // Check last input which has an error or not to re assign values and reset error
      // if (this.isbnListResult[id - 1].error.flag) {
      //   this.isbnListResult[id].error.flag = false;
      //   this.isbnListResult[id].product_id = '';

      //   this.isbnListResult[id - 1].number = this.getIsbnValue(id - 1);
      //   this.isbnListResult[id - 1].qty = this.getQtyValue(id - 1);
      // }

      const pastedText =  this.getClipboardData(event.clipboardData);
      // Only allow to paste multi items on the opened input section
      if (!this.isbnListResult[id].error.flag && this.isbnListResult[id].product_id === '') {
        this.handleClipboardData(id, pastedText);
      } else {
        const pastedIsbnInfo = pastedText.split(/\n/g).filter(Boolean);
        if (pastedIsbnInfo.length === 1 && pastedIsbnInfo[0].replace(/\s/g, '') !== '') {
          const pastedSingleIsbn = pastedIsbnInfo[0].split(/\t/g).filter(Boolean);
          if (pastedSingleIsbn.length === 1) {
            this.updateSubTotalWithIsbnUpdate(id);
            this.treatAsSingleIsbn(id, pastedSingleIsbn[0]);
          } else {
            this.isbnListResult[id].error = {
              flag: true,
              message: this.typeIsbnErrorMsg
            };
          }
        } else if (pastedIsbnInfo.length > 1) {
          this.isbnListResult[id].error = {
            flag: true,
            message: 'Cannot paste multiple items.'
          };
        } else {
          this.isbnListResult[id].error = {
            flag: true,
            message: this.typeIsbnErrorMsg
          };
        }
      }
    }

    // Reset submitted to activate 'Add to Cart'
    this.submitted = false;
  }

  private handleClipboardData(id: number, pastedText: any) {
    let secondISBN: any = [];
    secondISBN = pastedText.split(/\n/g).filter(Boolean);
    if (secondISBN[0].replace(/\s/g, '') !== '') {
      secondISBN.push(this.extraEmptyGroup);
      this.processIsbn(secondISBN, true);

      // Remove a group from the list by id
      this.isbnListForms.removeAt(id);
      this.isbnListResult.splice(id, 1);
    }
  }

  private processSingle(id: number) {
    const isbnValue = this.getIsbnValue(id);
    if (this.checkIsbnLength(isbnValue)) {
      this.getProductInfo(id, isbnValue);
    } else {
      this.isbnListResult[id].error = {
        flag: true,
        message: 'Incorrect ISBN 2'
      };

      // Update product info and status
      this.updateProductInfo(id, [], this.isbnListResult[id].error);

      const isbnAvailable = this.document.getElementById('isbnAvailable_' + id);
      if (isbnAvailable) {
        this.document.getElementById('isbnAvailable_' + id).remove();
      }
    }
  }

  private getProductInfo(id: number, isbnValue: string) {
    const getValues = '?storeID=' + this.miniBC.storeID + '&token=' + this.miniBC.token + '&isbn=' + isbnValue;
    this.connection.get(this.miniBC.url + '/apps/nelson/storefront/getProductInfo' + getValues).subscribe(data => {
      if ('error' in data) {
        this.isbnListResult[id].info = [];
        if (!this.isbnListResult[id].error.flag) {
          this.isbnListResult[id].error = {
            flag: true,
            message: isbnValue + ' does not match with any ISBNs.'
          };
        }
      } else {
        this.isbnListResult[id].product_id = data.product_id;
        this.isbnListResult[id].number = isbnValue;
        this.isbnListResult[id].info = data;
        this.isbnListResult[id].error = {
          flag: false,
          message: '5'
        };

        const qty = this.getQtyValue(id);
        if (parseInt(data.available_qty, 10) < qty) {
          data.backOrder = qty - data.available_qty;
        }

        this.setRestrictedItem(id, data);

        // Update product info and status
        this.updateProductInfo(id, data, this.isbnListResult[id].error);

        if (this.getQtyValue(id) > 0) {
          this.updateSubTotal(data.net_price, this.getQtyValue(id), this.isbnListResult[id].info.availability);
        }
      }
    });
  }

  private treatAsSingleIsbn(id: number, isbnValue: string) {
    if (this.checkIsbnLength(isbnValue)) {
      if (this.isbnListResult[id].error) {
        this.isbnListResult[id].error = {
          flag: false,
          message: ''
        };
      }

      this.isbnListResult[id].number = isbnValue;
      this.isbnListResult[id].qty = '';
      this.getProductInfo(id, isbnValue);

      const items = this.form.get('isbnLists') as FormArray;
      const temp = (items).at(id);
      temp.patchValue({
        quantity: ''
      });

      this.moveFocus('isbnQty_' + id);
    } else {
      this.isbnListResult[id].error = {
        flag: true,
        message: this.incorrectLengthErrorMsg
      };
    }
  }

  /**
   *
   */
  private updateProductInfo(id: number, data: any, status: any) {
    // Update product info and status
    const items = this.form.get('isbnLists') as FormArray;
    const temp = (items).at(id);
    temp.patchValue({
      product_id: data.product_id,
      info: data,
      status: status
    });
  }

  /**
   * Add Product available text and update the subtotal
   */
  public addAvailability(id: number) {
    if (this.isbnListResult[id].info.availability === '') {
      this.isbnListResult[id].info.availability = 'Shipping time unavailable';
    }

    return this.isbnListResult[id].info.availability;
  }

  public setRestrictedItem(id: number, data: any) {
    switch (true) {
      case data.availability === 'No stock available':
          this.isbnListResult[id].restrictedText = 'This product is out of stock. Please <a href="/contact-us">contact us</a>';
          this.isbnListResult[id].restrictedStatus = true;
          break;
      case this.restrictedCodes.indexOf(data.rights_code) > -1:
          this.isbnListResult[id].restrictedText = 'Not available online. Please <a href="/contact-us">contact us</a>';
          this.isbnListResult[id].restrictedStatus = true;
          break;
      case data.rights_code === 'NS1':
        this.productRestriction(this.customerId).subscribe((result: any) => {
          if (typeof result.pricing_group !== 'undefined' && result.pricing_group === '441586') {
            this.isbnListResult[id].restrictedText = '';
            this.isbnListResult[id].restrictedStatus = false;
          } else {
            this.isbnListResult[id].restrictedText = 'Not available to your institution. Product Status: NS1';
            this.isbnListResult[id].restrictedStatus = true;
          }
        });
        break;
      default:
        this.isbnListResult[id].restrictedText = '';
        this.isbnListResult[id].restrictedStatus = false;
        break;
    }
  }

  public updateItemSubTotal(id: number) {
    if (parseInt(this.isbnListResult[id].qty, 10) > 0 && !this.isbnListResult[id].restrictedStatus) {
      return this.isbnListResult[id].info.net_price * this.isbnListResult[id].qty;
    }
  }

  /**
   * Calculate pre subtotal amount
   */
  public calculatePreSubTotal(event: any, id: number) {
    if (event.key > -1 || event.key < 10 || event.key === 'Delete' || event.key === 'Backspace') {
      // Get existing Qty and item subTotal
      const qty = this.getQtyValue(id);
      const preAmount = this.document.getElementById('preSubTotal[' + id + ']').textContent;

      // Reset error flag based on quantity entered
      if (this.isbnListResult[id].error.message === this.quantityErrorMsg && qty > 0) {
        this.isbnListResult[id].error = {
          flag: false,
          message: ''
        };
      }

      if (qty > -1 && preAmount !== '' && !this.isbnListResult[id].error.flag) {
        // Get only numbers with decimal from item subTotal amount string
        const tempAmount = this.getPlainPreSubAmount(preAmount);

        this.subtractTotal(parseFloat(tempAmount));
      }

      if (qty > -1 && !this.isbnListResult[id].error.flag && this.isbnListResult[id].info !== '') {
        // If no product info available, set price to be 0
        if (!this.isbnListResult[id].info.hasOwnProperty('net_price')) {
          this.isbnListResult[id].info.net_price = 0;
        }

        const itemSubTotal = this.isbnListResult[id].info.net_price * qty;
        this.addSubTotal(itemSubTotal, id);
      }

      // Need to assign qty
      this.isbnListResult[id].qty = qty;

      // Check the qty from available qty to set back order status
      if (parseInt(this.isbnListResult[id].info.available_qty, 10) < qty) {
        this.isbnListResult[id].info.backOrder = qty - this.isbnListResult[id].info.available_qty;
      } else {
        delete this.isbnListResult[id].info.backOrder;
      }

      // Reset submitted to activate 'Add to Cart'
      this.submitted = false;
    }
  }

  private productRestriction(id: number) {
    return new Observable((observer) => {
      const post = {
        storeID: this.miniBC.storeID,
        token: this.miniBC.token,
        uid: id
      };
      this.connection.post(this.miniBC.url + '/customer/apps/sm/smStorefront/productRestriction', post).subscribe(result => {
        if (result.success === true) {
          observer.next(result.data[0]);
          observer.complete();
        } else {
          observer.next();
          observer.complete();
        }
      }, error => {
        observer.next();
        observer.complete();
      });
    });
  }

  private checkIsbnLength(data) {
    return data.length === 10 || data.length === 13;
  }

  private getClipboardData(data) {
    let pastedText = '';
    this.windowPasteEvent = false;

    if (typeof data === 'undefined') {
      pastedText = window['clipboardData'].getData('text');
    } else {
      pastedText = data.getData('text');
    }

    return pastedText;
  }

  private checkKeyCode(code) {
    // 8 / 46 - delete 88 - Xx  189 - Hyphen 32 - space
    const allowedKeyCode = [8, 32, 46, 88, 189, 109, 173, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];

    return (code >= 48 && code <= 57) || allowedKeyCode.indexOf(code) > -1;
  }

  private getPlainPreSubAmount(preAmount) {
    const tempAmount = preAmount.replace('$', '');

    return tempAmount.replace(/,/g, '');
  }

  private updateSubTotal(amount: number, qty: number, id: number) {
    if (qty > 0) {
      this.addSubTotal(amount * qty, id);
    }
  }

  private updateSubTotalWithIsbnUpdate(id: number) {
    const preAmount = this.document.getElementById('preSubTotal[' + id + ']').textContent;
    // Check id not to allow deleting more than once
    if (preAmount && this.calculatedId.indexOf(id) === -1) {
      const tempAmount = this.getPlainPreSubAmount(preAmount);

      this.subtractTotal(parseFloat(tempAmount));
      this.calculatedId.push(id);
    }
  }

  /**
   * Add amount to subtotal after calculation
   */
  private addSubTotal(amount: number, id: number) {
    setTimeout(() => {
      if (!this.isbnListResult[id].restrictedStatus) {
        this.calculatedId = [];
        this.subTotal += amount;
      }
    }, 200);
  }

  /**
   * Subtraction removing items' amount from subtotal
   */
  private subtractTotal(amount: number) {
    this.subTotal -= amount;
  }

  /**
   * Close Tip Box
   */
  public closeTipBox() {
    this.document.getElementById('tipBox').remove();
  }

  /**
   * Get submitted data
   */
  public onSubmit() {
    // To filter out empty info and invalid isbn
    let totalCartQuantity = 0;
    const preCartItems: any = [];
    let quantityStatus = true;
    this.form.value.isbnLists.forEach((items: any, index: number) => {
      if (items.isbn !== '' && items.quantity === '' && !this.isbnListResult[index].error.flag) {
        this.isbnListResult[index].error = {
          flag: true,
          message: this.quantityErrorMsg
        };

        quantityStatus = false;
      }

      if (this.isbnListResult[index].error.flag && this.isbnListResult[index].error.message === this.quantityErrorMsg) {
        quantityStatus = false;
      }

      if (typeof items.isbn === 'undefined' ||
        (items.product_id !== null && items.isbn !== '' && items.quantity !== '' && items.status.flag === false && !this.isbnListResult[index].restrictedStatus)) {
        if (typeof items.isbn !== 'undefined') {
          delete items.status;
          delete items.info;
          delete items.isbn;
        }
        totalCartQuantity += parseInt(items.quantity, 10);
        preCartItems.push(items);
      }
    });

    console.log('preCartItems', preCartItems, 'TotalQuantity', totalCartQuantity);

    if (!this.submitted && quantityStatus) {
      this.cart.inCart(preCartItems).subscribe((result: any) => {
        const tempCartQuantity = this.document.getElementsByClassName('cart-quantity');
        for (const item of tempCartQuantity) {
          item.className += ' ' + 'countPill--positive';
          item.textContent = totalCartQuantity + parseInt(item.textContent, 10);
        }
      });

      this.submitted = true;

      setTimeout(() => {
        this.submitted = false;
        this.subTotal = 0;
        this.isbnListResult = [];
        this.ngOnInit();
      }, 2000);
    }
  }
}
