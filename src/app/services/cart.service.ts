import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  public cart: any = {};
  public cart_request_status = false;
  public cart_request_message: string;

  constructor(
    private connection: ConnectionService
  ) { }

  // This function will try and get the cart, if it cant find a cart then it will creat one.
  public inCart(items: any): Observable<any> {
    return new Observable((observer) => {
      this.getCartStorefront().subscribe((result: any) => {
        // if the cart is not there then create a new cart
        if (result.error) {
          this.createCart(items).subscribe(cart_id => {
            // Get the cart we just created from the server.
            this.getCartServer(cart_id).subscribe((cartServerResult: any) => {
              observer.next(cartServerResult);
              observer.complete();
            });
          }, error => {
              observer.next(error);
              observer.complete();
          });
        } else {
            // Set or update the subscription plan
            this.addToCart(items).subscribe(addToCartResult => {
                observer.next(addToCartResult);
                observer.complete();
            });
        }
      });
    });
  }

  // This function uses the storefront cart API
  // This function will get the current cart.
  public getCartStorefront(): Observable<any> {
    return new Observable((observer) => {
        const url = '/api/storefront/carts?include=lineItems.digitalItems.options,lineItems.physicalItems.options';
        this.connection.get(url).subscribe((cart: any) => {
            if (cart.length === 0) {
                observer.next({error: true, msg: 'no cart found', data: this.cart});
                observer.complete();
            } else {
              // The format from the storefront API returns different variable
              // names so I get the cart again with the server to server API
              // once I have the cart ID.
              this.getCartServer(cart[0].id).subscribe(result => {
                this.cart.id = cart[0].id;
                observer.next({ error: false, msg: 'cart found', data: cart[0].id});
                observer.complete();
              });
            }
        }, error => {
            observer.error(error);
        });
    });
  }

  // This function uses the server to server cart API
  // This function will get the current cart.
  public getCartServer(cartId): Observable<any> {
    return new Observable((observer) => {
        const url = 'https://www.ribon.ca/api/live/7l4m3t4wzb/get';
        const data = {
          uri: '/v3/carts/' + cartId + '?include=line_items.digital_items.options,line_items.physical_items.options',
          query: ''
        };

        this.connection.post(url, data).subscribe((result: any) => {
            observer.next(result);
            observer.complete();
        }, error => {
            observer.error(error);
        });
    });
  }

  // This function uses the storefront cart API
  // This function will create a big commerce cart.
  public createCart(items: any): Observable<any> {
    return new Observable((observer) => {
        const data = JSON.stringify({lineItems: items});

        const xhr = new XMLHttpRequest();
        xhr.withCredentials = true;

        xhr.addEventListener('readystatechange', function () {
          if (this.readyState === this.DONE) {
            const new_cart = JSON.parse(this.responseText);
              observer.next(new_cart.id);
              observer.complete();
          }
        });

        xhr.open('POST', '/api/storefront/carts?include=lineItems.digitalItems.options,lineItems.physicalItems.options');
        xhr.setRequestHeader('content-type', 'application/json');

        xhr.send(data);
    });
  }

  // This function uses the server to server cart API
  // This function will add to a big commerce cart.
  public addToCart(items: any): Observable<any> {
    return new Observable((observer) => {
        if (items.length > 0) {
            observer.next('No items to add');
            observer.complete();
        }

        if (this.cart.id !== undefined) {
            const url = 'https://www.ribon.ca/api/live/7l4m3t4wzb/post';
            const data = {
              uri: '/v3/carts/' + this.cart.id + '/items?include=line_items.digital_items.options,line_items.physical_items.options',
              query: { line_items: items }
            };

            this.connection.post(url, data).subscribe((result: any) => {
                observer.next(result);
                observer.complete();
            }, error => {
                observer.error(error);
            });
        } else {
            observer.next('No cart id.');
            observer.complete();
        }
    });
  }
}
