import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpParameterCodec } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ConnectionService {
  headers: any = false;

  constructor(
    private client: HttpClient
  ) {
    this.headers = new HttpHeaders().set('Content-Type', 'application/json');
  }


  get(url): Observable<any> {
    return this.client.get(url);
  }

  post(url, query): Observable<any> {
    let params = new HttpParams({ encoder: new WebHttpUrlEncodingCodec() });

    params = this.parseParams(params, query, []);

    return this.client.post(url, params, this.headers);
  }

  delete(url): Observable<any> {
    return this.client.delete(url, this.headers);
  }

  getPayload(url, query): Observable<any> {
    return this.client.get(url, this.headers);
  }

  postPayload(url, query): Observable<any> {
    return this.client.post(url, query, this.headers);
  }

  putPayload(url, query): Observable<any> {
    return this.client.put(url, query, this.headers);
  }

  public serialize(query: any, prefix: any): any {
    const str = [];
    for(const p in query) {
      if (query.hasOwnProperty(p)) {
        const k = prefix ? prefix + '[' + p + ']' : p, v = query[p];
        str.push((v !== null && typeof v === 'object') ?
          this.serialize(v, k) :
          k + '=' + encodeURIComponent(v));
      }
    }

    return str.join('&');
  }

  private parseParams(params, query, depth) {
    for(const q in query) {
      if (typeof query[q] === 'object') {
        const newDepth = [];
        for(let d = 0; d < depth.length; d++) {
          newDepth.push(depth[d]);
        }
        newDepth.push(q);
        params = this.parseParams(params, query[q], newDepth);
      } else {
        let s = q;
        for (let d = 0; d < depth.length; d++) {
          if (d > 0) {
            s += '[' + depth[d] + ']';
          } else {
            s = depth[d];
          }

          if (d === depth.length - 1) {
            s += '[' + q + ']';
          }
        }

        params = params.append(s, query[q]);
      }
    }
    return params;
  }
}

class WebHttpUrlEncodingCodec implements HttpParameterCodec {
  encodeKey(k: string): string { return encodeURIComponent(k); }
  encodeValue(v: string): string { return encodeURIComponent(v); }
  decodeKey(k: string): string { return decodeURIComponent(k); }
  decodeValue(v: string) { return decodeURIComponent(v); }
}
