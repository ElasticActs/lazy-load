import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// material components
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatChipsModule,
  MatExpansionModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatSliderModule,
  MatSnackBarModule,
  MatDialogModule,
  MatToolbarModule,
  MatRippleModule,
  MatAutocompleteModule,
  MatTabsModule,
  MatSelectModule,
  MatCheckboxModule,
  MatRadioModule,
  MatTooltipModule,
  MatSlideToggleModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatIconModule,
  MatProgressBarModule,
  MatPaginatorModule,
  MatButtonModule,
  MatListModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule
} from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { CdkTableModule } from '@angular/cdk/table';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Location } from '@angular/common';

// App
import { HomeComponent } from './home/home.component';

// services
import { ConnectionService } from './services/connection.service';
import { WindowRef } from './services/window.service';
import { CartService } from './services/cart.service';

// Pipes
import { TrustAsHtmlPipe } from './pipes/trustAsHtml.pipe';
import { NumberSuffixPipe } from './pipes/numberSuffix.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatChipsModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatSliderModule,
    MatSnackBarModule,
    MatDialogModule,
    MatToolbarModule,
    MatRippleModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatIconModule,
    MatProgressBarModule,
    MatPaginatorModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    AppRoutingModule,
    CdkTableModule
  ],
  providers: [TrustAsHtmlPipe, NumberSuffixPipe, CartService, ConnectionService, WindowRef],
  bootstrap: [AppComponent]
})
export class AppModule { }

// const trailingSlash = (Location as any).stripTrailingSlash;
// (Location as any).stripTrailingSlash = function _stripTrailingSlash(url: string): string {
//   if (url.endsWith('/')) {
//     url = url;
//   } else {
//     url = url + '/';
//   }

//   const queryString$ = url.match(/([^?]*)?(.*)/);
//   if (queryString$[2].length > 0) {
//     return /[^\/]\/$/.test(queryString$[1]) ? queryString$[1] + '.' + queryString$[2] : trailingSlash(url);
//   }

//   return /[^\/]\/$/.test(url) ? url + '.' : trailingSlash(url);
// };
