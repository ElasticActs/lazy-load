# Redbergs

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Re set git repository

> git remote show origin
> find . | grep .git | xargs rm -rf
> git init
> git remote show origin
> git remote add origin https://kenu717@bitbucket.org/bapoteam/creative-bag-salesmanager.git
> git remote show origin


## Change "origin" of your GIT repository
> git remote rm origin
> git remote add origin git@github.com:aplikacjainfo/proj1.git
> git config master.remote origin
> git config master.merge refs/heads/master
